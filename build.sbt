import com.typesafe.sbt.SbtNativePackager._
import NativePackagerKeys._

name := """mars-rover-scala-play"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.10.2"

libraryDependencies += "org.scalatest" %% "scalatest" % "2.0.M5b" % "test"

libraryDependencies += "junit" % "junit" % "4.10" % "test"

libraryDependencies += "net.liftweb" %% "lift-json" % "2.6-M4"

libraryDependencies += "com.google.code.gson" % "gson" % "2.2"

packageArchetype.java_application