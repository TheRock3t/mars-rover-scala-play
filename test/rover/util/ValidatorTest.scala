package rover.util

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import rover.util.Validator._

/**
 * Test for rover.util.Validator
 * @author Mihai Anghel
 */
@RunWith(classOf[JUnitRunner])
class ValidatorTest extends FunSuite {

  /**
   * Tests to validate grid coordinates
   */
  test("Correct grid coordinates") {
    val gridCoordinates = validateGridCoordinates(" 5", " 5")
    assert(gridCoordinates === (5, 5))
  }

  test("Empty grid coordinates") {
    val thrown = intercept[IllegalArgumentException] {
      validateGridCoordinates(" ", "")
    }
    assert(thrown.getMessage === "Wrong input parameter [length] !")
  }

  test("Wrong grid coordinates: length") {
    val thrown = intercept[IllegalArgumentException] {
      validateGridCoordinates("A ", "6")
    }
    assert(thrown.getMessage === "Wrong input parameter [length] !")
  }

  test("Wrong grid coordinates: width") {
    val thrown = intercept[IllegalArgumentException] {
      validateGridCoordinates("5", " B")
    }
    assert(thrown.getMessage === "Wrong input parameter [width] !")
  }

  test("Wrong grid coordinates: negative length") {
    val thrown = intercept[IllegalArgumentException] {
      validateGridCoordinates("-4 ", "6")
    }
    assert(thrown.getMessage === "Wrong input parameter [length] !")
  }

  test("Wrong grid coordinates: negative width") {
    val thrown = intercept[IllegalArgumentException] {
      validateGridCoordinates("5", " -6")
    }
    assert(thrown.getMessage === "Wrong input parameter [width] !")
  }

  test("Wrong grid coordinates: too many values") {
    val thrown = intercept[IllegalArgumentException] {
      validateGridCoordinates("5 6", " 7")
    }
    assert(thrown.getMessage === "Wrong input parameter [length] !")
  }

  /**
   * Tests to validate rover coordinates
   */
  test("Correct rover coordinates") {
    val roverCoordinates = validateRoverInitialCoordinates("1", "3", "N", 4, 4)
    assert(roverCoordinates === (1, 3, Coordinates.N))
  }

  test("Empty rover coordinates") {
    val thrown = intercept[IllegalArgumentException] {
      validateRoverInitialCoordinates(" ", "", " ", 4, 4)
    }
    assert(thrown.getMessage === "Wrong input parameter [x] !")
  }

  test("Wrong rover coordinates: position x") {
    val thrown = intercept[IllegalArgumentException] {
      validateRoverInitialCoordinates(" d", "4", "N", 4, 4)
    }
    assert(thrown.getMessage === "Wrong input parameter [x] !")
  }

  test("Wrong rover coordinates: position y") {
    val thrown = intercept[IllegalArgumentException] {
      validateRoverInitialCoordinates(" 3", " d", "N", 4, 4)
    }
    assert(thrown.getMessage === "Wrong input parameter [y] !")
  }

  test("Wrong rover coordinates: orientation") {
    val thrown = intercept[IllegalArgumentException] {
      validateRoverInitialCoordinates(" 3",  "4 ", "U", 4, 4)
    }
    assert(thrown.getMessage === "Wrong input parameter [o] !")
  }
  
  test("Wrong rover coordinates: outside the grid on x") {
    val thrown = intercept[IllegalArgumentException] {
      validateRoverInitialCoordinates(" 5", "4", "N", 4, 4)
    }
    assert(thrown.getMessage === "Rover is outside the grid at position x=5 !")
  }

  test("Wrong rover coordinates: outside the grid on y") {
    val thrown = intercept[IllegalArgumentException] {
      validateRoverInitialCoordinates(" 3", " -5", "N", 4, 4)
    }
    assert(thrown.getMessage === "Rover is outside the grid at position y=-5 !")
  }
}