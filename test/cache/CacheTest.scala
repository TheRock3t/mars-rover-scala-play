package cache

import rover.Rover
import rover.util.Coordinates._
import scala.collection.mutable.HashMap
import org.junit.{Before, Test}
import org.scalatest.junit.AssertionsForJUnit

/**
 * Test for cache.Cache
 * @author Mihai Anghel
 */
class CacheTest extends AssertionsForJUnit {

  @Before
  def deleteCache = {
    Cache.invalidateCache
  }

  @Test
  def returnEmptyList() = {
    assert(Cache.returnRovers.size == 0)
  }

  @Test
  def returnListOfRovers() = {
    Cache.addRover("Rovy", new Rover(1, 2, N))
    Cache.addRover("Novy", new Rover(4, 2, E))
    val result = Cache.returnRovers match {
      case Some(rover) => rover
      case _ => emptyMap
    }
    assert(result.size === 2)
  }

  @Test
  def returnSpecificRover() = {
    Cache.addRover("Rovy", new Rover(4, 2, E))
    Cache.returnRover("Rovy") match {
      case Some(rover) => assert(rover.isInstanceOf[Rover])
      case _ => assert(false)
    }
  }

  @Test
  def nothingIsReturnedIfNoRoverIsAdded() = {
    Cache.returnRover("Rovy") match {
      case None => assert(true)
      case _ => assert(false)
    }
  }

  @Test
  def addSpecificRover() = {
    Cache.addRover("Rovy", new Rover(4, 2, E))
    Cache.returnRover("Rovy") match {
      case Some(rover) => {
        assert(rover.isInstanceOf[Rover])
        assert(rover.x == 4)
        assert(rover.y == 2)
        assert(rover.o == E)
      }
      case _ => assert(false)
    }
  }

  @Test
  def deleteARover() = {
    Cache.addRover("Rovy", new Rover(1, 2, N))
    Cache.deleteRover("Rovy")
    assert(Cache.returnRovers.size === 0)
  }

  @Test
  def returnUnsetGridSizes() = {
    Cache.returnGridSize match {
      case None => assert(true)
      case _ => assert(false)
    }
  }

  @Test
  def editGridSizes() = {
    Cache.editGridSize((3, 3))
    Cache.returnGridSize match {
      case Some(size) => {
        assert(size.isInstanceOf[Tuple2[Int, Int]])
        assert(size._1 == 3)
        assert(size._2 == 3)
      }
      case _ => assert(false)
    }
  }

  private def emptyMap() = HashMap[String, Rover]()
}