package cache

import rover.Rover
import rover.util.Coordinates
import scala.collection.mutable.HashMap
import com.google.common.annotations.VisibleForTesting

/**
 * In-memory cache for storing rovers and grid size
 * @author Mihai Anghel
 */
object Cache {

  private val roverCache = new HashMap[String, Rover]()

  private val gridSize = HashMap("x" -> 0, "y" -> 0)

  /**
   * Returns all the stored rovers
   */
  def returnRovers(): Option[HashMap[String, Rover]] = if (roverCache.isEmpty) None else Some(roverCache)

  /**
   * Returns one rover by name
   */
  def returnRover(name: String): Option[Rover] = if (roverCache.isEmpty) None else roverCache.get(name)

  /**
   * Adds a rover to the cache
   */
  def addRover(name: String, r: Rover): Unit = roverCache.+=((name, r))

  /**
   * Delete a rover from the cache
   */
  def deleteRover(name: String): Unit = roverCache.-=(name)

  /**
   * Edit grid size. Initially is (0, 0)
   */
  def editGridSize(coord: (Int, Int)): Unit = {
    if (coord._1 != 0 && coord._2 != 0) gridSize.+=(("x", coord._1), ("y", coord._2))
  }

  /**
   * Return the grid size
   */
  def returnGridSize(): Option[Tuple2[Int, Int]] = {
    val lenght = gridSize.get("x") match {
      case Some(int) => int
      case None => 0
    }
    val width = gridSize.get("y") match {
      case Some(int) => int
      case None => 0
    }
    if (lenght == 0 || width == 0) None else Some((lenght, width))
  }

  /**
   * Method to invalidate cache
   */
  @VisibleForTesting
  def invalidateCache: Unit = {
    roverCache.clear
    gridSize.+=(("x", 0), ("y", 0))
  }
}