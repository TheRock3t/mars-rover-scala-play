package controllers

import play.api.mvc.{ Controller, Action }
import rover.util.{ Coordinates, Validator }
import play.api.libs.json.Json
import cache.Cache
import rover.Rover

/**
 * Controller for the managing the requests for grid size management
 * @author Mihai Anghel
 */
object GridController extends Controller {
  
  def returnGridSize = Action {
    Cache.returnGridSize match {
      case Some(grid) => Ok(Json.toJson(Map("length" -> grid._1, "width" -> grid._2)))
      case _ => BadRequest("Grid size was not set !")
    }
    
  }

  def editGridSize = Action(parse.json) { request =>
    (request.body \ "length").asOpt[String].map { length => {
        (request.body \ "width").asOpt[String].map { width => {
            try {
              val coord = Validator.validateGridCoordinates(length, width)
              Cache.editGridSize(coord);
              Ok("Grid size updated successfully !")
            } catch {
              case e: IllegalArgumentException => BadRequest(e.getMessage())
              case e: IllegalStateException => InternalServerError(e.getMessage())
            }

          }
        }.getOrElse { BadRequest("Missing parameter [width]") }
      }
    }.getOrElse { BadRequest("Missing parameter [length]") }
  }

}