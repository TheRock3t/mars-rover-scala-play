package controllers

import play.api.mvc.{ Controller, Action }
import play.api.libs.iteratee.Enumerator
import play.api.libs.json.{ Json, JsValue }
import play.api.mvc.BodyParsers.parse
import rover.util.{ Coordinates, Validator }
import rover.Rover
import cache.Cache
import play.api.mvc.Result

/**
 * Controller for the managing the requests for rover management
 * @author Mihai Anghel
 */
object RoverController extends Controller {

  def returnRovers() = Action {
    Cache.returnRovers match {
      case Some(seqOfRovers) => Ok(Json.toJson(seqOfRovers.toMap))
      case None => Ok("No rovers added !")
    }
  }

  def addRover() = Action(parse.json) { request =>
    (request.body \ "name").asOpt[String].map { name =>
      {
        (request.body \ "x").asOpt[String].map { x =>
          {
            (request.body \ "y").asOpt[String].map { y =>
              {
                (request.body \ "o").asOpt[String].map { o =>
                  {
                    try {
                      Cache.returnGridSize match {
                        case Some(grid) => {
                          val coords = Validator.validateRoverInitialCoordinates(x, y, o, grid._1, grid._2)
                          Cache.addRover(name, new Rover(coords._1, coords._2, coords._3))
                          Ok("Rover added successfully !")
                        }
                        case _ => BadRequest("Grid size was not set !")
                      }
                    } catch {
                      case e: IllegalArgumentException => BadRequest(e.getMessage())
                      case e: IllegalStateException => InternalServerError(e.getMessage())
                    }
                  }
                }.getOrElse { BadRequest("Missing parameter [o]") }
              }
            }.getOrElse { BadRequest("Missing parameter [y]") }
          }
        }.getOrElse { BadRequest("Missing parameter [x]") }
      }
    }.getOrElse { BadRequest("Missing parameter [name]") }
  }

  def deleteRover() = Action(parse.json) { request =>
    (request.body \ "name").asOpt[String].map { name =>
      {
        try {
          Cache.deleteRover(name)
          Ok("Rover deleted successfully !")
        } catch {
          case e: IllegalArgumentException => BadRequest(e.getMessage())
          case e: IllegalStateException => InternalServerError(e.getMessage())
        }
      }
    }.getOrElse { BadRequest("Missing parameter [name]") }
  }

  def sendCommand() = Action { request =>
    request.queryString.get("name").flatMap(_.headOption).map { name =>
      {
        request.queryString.get("c").flatMap(_.headOption).map { c =>
          {
            try {
              Cache.returnRover(name) match {
                case Some(rover) => {
                  Cache.returnGridSize match {
                    case Some(grid) => {
                      val newPosRover = rover.asInstanceOf[Rover].move(c)(grid._1, grid._2)
                      Cache.addRover(name, newPosRover);
                      Ok("New position for %s is %s".format(name, newPosRover))
                    }
                    //This won't happen because you can't have a rover without having the grid size set
                    case _ => BadRequest("Grid size was not set !")
                  }
                }
                case _ => BadRequest("There is no rover with name=%s".format(name))
              }
            } catch {
              case e: IllegalArgumentException => BadRequest(e.getMessage())
              case e: IllegalStateException => InternalServerError(e.getMessage())
            }
          }
        }.getOrElse { BadRequest("Missing parameter [c]") }
      }
    }.getOrElse { BadRequest("Missing parameter [name]") }
  }
}