package rover

import rover.util.Coordinates._
import play.api.libs.json.Format
import play.api.libs.json.JsValue
import play.api.libs.json.JsString
import play.api.libs.json.JsResult
import play.api.libs.json.JsSuccess
import play.api.libs.json.JsObject

/**
 * Model class for rover
 * @author Mihai Anghel
 */
case class Rover(val x: Int, val y: Int, val o: Coordinates) {

  if (x < 0) {
    throw new IllegalStateException("Rover is out of the grid at position x=%d y=%d !".format(x, y))
  }

  if (y < 0) {
    throw new IllegalStateException("Rover is out of the grid at position x=%d y=%d !".format(x, y))
  }

  require((o == N) || (o == S) || (o == W) || (o == E))

  /**
   * Moves the rover according to the commands parameter
   */
  def move(commands: String)(implicit sizeOfGrid: (Int, Int)): Rover = {

    def movement(listOfCmds: List[Char], r: Rover)(implicit sizeOfGrid: (Int, Int)): Rover = listOfCmds match {
      case Nil => r
      case head :: tail => head match {
        case 'L' => r.o match {
          case N => movement(tail, new Rover(r.x, r.y, W))
          case W => movement(tail, new Rover(r.x, r.y, S))
          case S => movement(tail, new Rover(r.x, r.y, E))
          case E => movement(tail, new Rover(r.x, r.y, N))
          case _ => throw new IllegalArgumentException()
        }
        case 'R' => r.o match {
          case N => movement(tail, new Rover(r.x, r.y, E))
          case E => movement(tail, new Rover(r.x, r.y, S))
          case S => movement(tail, new Rover(r.x, r.y, W))
          case W => movement(tail, new Rover(r.x, r.y, N))
          case _ => throw new IllegalArgumentException()
        }
        case 'M' => r.o match {
          case N => movement(tail, Rover.checkIfRoverIsOutOfBounds(new Rover(r.x, r.y + 1, r.o)))
          case E => movement(tail, Rover.checkIfRoverIsOutOfBounds(new Rover(r.x + 1, r.y, r.o)))
          case S => movement(tail, Rover.checkIfRoverIsOutOfBounds(new Rover(r.x, r.y - 1, r.o)))
          case W => movement(tail, Rover.checkIfRoverIsOutOfBounds(new Rover(r.x - 1, r.y, r.o)))
          case _ => throw new IllegalArgumentException()
        }
        case _ => throw new IllegalArgumentException("Unknown command!")
      }
    }

    movement(commands.toList, this)
  }

  override def toString() = "%d %d %s".format(x, y, o)
}

object Rover {

  implicit object RoverFormat extends Format[Rover] {

    // convert from Rover object to JSON (serializing to JSON)
    def writes(rover: Rover): JsValue = {
      val roverSeq = Seq(
        "x" -> JsString(rover.x.toString),
        "y" -> JsString(rover.y.toString),
        "o" -> JsString(rover.o.toString))
      JsObject(roverSeq)
    }

    // convert from JSON string to a Rover object (de-serializing from JSON)
    def reads(json: JsValue): JsResult[Rover] = {
      JsSuccess(Rover(1, 2, N))
    }
  }

  /**
   * Checks if the rover is out of bounds according to the sizeOfGrid parameter
   */
  def checkIfRoverIsOutOfBounds(r: Rover)(implicit sizeOfGrid: (Int, Int)): Rover =
    if (r.x > sizeOfGrid._1 || r.y > sizeOfGrid._2) throw new IllegalStateException("Rover is out of the grid at position x=%d y=%d !".format(r.x, r.y)) else r

}