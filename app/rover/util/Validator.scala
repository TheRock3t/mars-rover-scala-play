package rover.util

/**
 * Validator utility class
 * @author Mihai Anghel
 */
object Validator {

  /**
   * Validates the grid coordinates
   */
  def validateGridCoordinates(x: String, y: String): (Int, Int) = {

    val length = toInt(x.trim()) match {
      case Some(int) if int >= 0 => int
      case _ => throw new IllegalArgumentException("Wrong input parameter [length] !")
    }
    val height = toInt(y.trim()) match {
      case Some(int) if int >= 0 => int
      case _ => throw new IllegalArgumentException("Wrong input parameter [width] !")
    }

    (length, height)

  }

  /**
   * Validates the rover initial coordinates
   */
  def validateRoverInitialCoordinates(xRover: String, yRover: String, c: String, xGrid: Int, yGrid: Int): (Int, Int, Coordinates.Value) = {

    val xCoord = toInt(xRover.trim()) match {
      case Some(int) => {
        if ((int < 0) || (int > xGrid)) throw new IllegalArgumentException("Rover is outside the grid at position x=%s !".format(int))
        else int
      }
      case _ => throw new IllegalArgumentException("Wrong input parameter [x] !")
    }
    val yCoord = toInt(yRover.trim()) match {
      case Some(int) => {
        if ((int < 0) || (int > yGrid)) throw new IllegalArgumentException("Rover is outside the grid at position y=%s !".format(int))
        else int
      }
      case _ => throw new IllegalArgumentException("Wrong input parameter [y] !")
    }

    getOrientationValue(c.trim()) match {
      case orientation: Coordinates.Value => (xCoord, yCoord, orientation)
      case e: Throwable => throw e
    }
  }

  /**
   * Converts the String to Option[Int]
   */
  def toInt(s: String): Option[Int] = {
    try {
      Some(s.toInt)
    } catch {
      case e: Exception => None
    }
  }

  /**
   * Gets the orientation value
   * If any error occurs, translates NoSuchElementException to IllegalArgumentException for consistency
   */
  def getOrientationValue(s: String): Any = {
    try {
      Coordinates.withName(s);
    } catch {
      case _: Throwable => new IllegalArgumentException("Wrong input parameter [o] !")
    }
  }
}